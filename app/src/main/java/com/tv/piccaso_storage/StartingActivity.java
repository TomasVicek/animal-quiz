package com.tv.piccaso_storage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.HashMap;

import static com.tv.piccaso_storage.MainActivity.SHARED_PREFERENCE;

public class StartingActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);

        Button gallery_button = findViewById(R.id.gallery_button);
        Button easy = findViewById(R.id.easy_button);
        final Button Intermediate = findViewById(R.id.intermediate_button);

        easy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(StartingActivity.this, MainActivity.class);

                i.putExtra("difficulty", "Easy");
                startActivity(i);
                finish();

            }}
        );


        Intermediate.setOnClickListener(new View.OnClickListener() {

            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
            public void onClick(View v) {
                Intent i = new Intent(StartingActivity.this, MainActivity.class);
                if(lvlacceess("Easy")){

                    i.putExtra("difficulty", "Intermediate");
                    startActivity(i);
                    finish();
                }
                else Toast.makeText(StartingActivity.this,"complete Easy lvl first",Toast.LENGTH_LONG).show();


            }}
        );

        gallery_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StartingActivity.this,MainGalleryActivity.class);
                startActivity(i);

            }
        });


    }

    private boolean lvlacceess(String difficulty) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);

        switch (difficulty) {

            case ("Easy"):
                return sharedPreferences.getBoolean("lvlEasyCompleted", false);
            case ("Intermediate"):
                return sharedPreferences.getBoolean("lvlIntermediateCompleted", false);

                }
        return false;

    }

}
