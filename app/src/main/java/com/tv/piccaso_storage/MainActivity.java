package com.tv.piccaso_storage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private String name;
    public String filename;
    TextView printName;
    TextView printFilename;
    public static final String SHARED_PREFERENCE = "sharedPreference";
    Integer onScreenLvl;
    EditText editText;
    ArrayList<Integer> lvl_history = new ArrayList<>();
    Integer max;
    Integer lvl;
    TextView textlvlNumber;
    ImageView image;
    TextView description;
    Button next;
    String difficulty;
    Button homeButton;
    Intent intent;
    Integer number_of_questions;
    final int zero=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.editText);
        Button reset = findViewById(R.id.reset);
        next = findViewById(R.id.next);
        Button before = findViewById(R.id.before);
        editText = findViewById(R.id.editText);
        textlvlNumber = findViewById(R.id.lvl);
        description = findViewById(R.id.description);
        homeButton = findViewById(R.id.home);



        intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            difficulty = bundle.getString("difficulty");
        }

        final Namedatabase answers = new Namedatabase();
        number_of_questions = answers.getAnswers(difficulty).length;
        switch(difficulty) {
            case "Easy":
                onScreenLvl = Integer.valueOf(loadData().get("onScreenLvlEasy").toString());
                lvl_history.add(Integer.valueOf(loadData().get("maxLvlEasy").toString()));
                Toast.makeText(this,onScreenLvl.toString(),Toast.LENGTH_LONG).show();

                break;

            case "Intermediate" :
                onScreenLvl = Integer.valueOf(loadData().get("onScreenLvlIntermediate").toString());
                lvl_history.add(Integer.valueOf(loadData().get("maxLvlIntermediate").toString()));
                Toast.makeText(this,onScreenLvl.toString(),Toast.LENGTH_LONG).show();

        }

        showImage(onScreenLvl);
        lvl = onScreenLvl + 1;
        textlvlNumber.setText(lvl.toString());
        description.setText(answers.getDescriptions(difficulty)[onScreenLvl][0]);




        lvl_history.add(Integer.valueOf(loadData().get("maxLvl"+difficulty).toString()));
        if (onScreenLvl<Collections.max(lvl_history)){
            editText.setText(answers.getAnswers(difficulty)[onScreenLvl][0]);
        }

        //Toast.makeText(this,"maxlvl"+Collections.max(lvl_history).toString()+"lvl"+onScreenLvl.toString(),Toast.LENGTH_SHORT).show();
        //next.setText(editText.getText().toString());
        reset.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View v) {
                                         onScreenLvl = 0;

                                         showImage(zero);
                                         Integer lvl = zero + 1;
                                         textlvlNumber.setText((lvl).toString());
                                         lvl_history.clear();
                                         lvl_history.add(0);
                                         editText.setText("");
                                         saveData();
                                         description.setText(answers.getDescriptions(difficulty)[onScreenLvl][0]);


                                     }
                                 }
        );


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (onScreenLvl < Collections.max(lvl_history)) {
                    lvl = onScreenLvl + 1;
                    textlvlNumber.setText((lvl).toString());
                    editText.setText(answers.getAnswers(difficulty)[onScreenLvl][0]);
                    description.setText(answers.getDescriptions(difficulty)[onScreenLvl][0]);

                    if (userAnswerEquals(editText.getText().toString())) {

                        onScreenLvl++;
                        updateScreen();
                        if (onScreenLvl < Collections.max(lvl_history))
                            editText.setText(answers.getAnswers(difficulty)[onScreenLvl][0]);
                        else {editText.setText("");}
                       //
                        //Toast.makeText(MainActivity.this, "lvl"+onScreenLvl.toString()+"max lvl"+Collections.max(lvl_history).toString(), Toast.LENGTH_SHORT).show();

                    }
                } else {


                    if (userAnswerEquals(editText.getText().toString())) {


                        if (onScreenLvl == (number_of_questions - 1)) {

                            Toast.makeText(MainActivity.this, "end", Toast.LENGTH_SHORT).show();
                            goToStartActiviy();
                        } else {

                            onScreenLvl++;
                            updateScreen();
                            editText.setText("");
                            //Toast.makeText(MainActivity.this, "lvl"+onScreenLvl.toString() + "maxlvl" +Collections.max(lvl_history).toString(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "wrong", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });


        before.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onScreenLvl == 0) {
                    Toast.makeText(MainActivity.this, "start", Toast.LENGTH_SHORT).show();
                } else {
                    onScreenLvl--;
                    updateScreen();
                    editText.setText(answers.getAnswers(difficulty)[onScreenLvl][0]);

                }
            }

        });
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,StartingActivity.class);
                startActivity(intent);
            }
        });

    }
    public boolean userAnswerEquals(String user_answer){
        final Namedatabase answers = new Namedatabase();

        for(int i=0;i<answers.getAnswers(difficulty)[onScreenLvl].length;i++)
            if (user_answer.equals(answers.getAnswers(difficulty)[onScreenLvl][i]))
                return true;
        return false;

    }


    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch(difficulty){
            case "Easy":editor.putInt("onScreenLvlEasy",onScreenLvl);
            editor.putInt("maxLvlEasy",Collections.max(lvl_history));
            if(Collections.max(lvl_history).equals(number_of_questions-1))
                editor.putBoolean("lvlEasyCompleted",true);

           // else editor.putBoolean("lvlEasyCompleted",false);
            editor.apply();

            break;


            case "Intermediate":editor.putInt("onScreenLvlIntermediate",onScreenLvl);
            editor.putInt("maxLvlIntermediate",Collections.max(lvl_history));
            if(Collections.max(lvl_history).equals(number_of_questions-1))
                editor.putBoolean("lvlIntermediateCompleted",true);


            editor.apply();
            break;


    }}

    public HashMap loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);


        HashMap<String,Integer> load =new HashMap<>();
        load.put("onScreenLvlEasy",sharedPreferences.getInt("onScreenLvlEasy",2));
        load.put("maxLvlEasy",sharedPreferences.getInt("maxLvlEasy",2));

        load.put("onScreenLvlIntermediate",sharedPreferences.getInt("onScreenLvlIntermediate",0));
        load.put("maxLvlIntermediate",sharedPreferences.getInt("maxLvlIntermediate",0));

        return load;

    }


    public void updateScreen(){
        final Namedatabase answers = new Namedatabase();
        description.setText(answers.getDescriptions(difficulty)[onScreenLvl][0]);
        lvl_history.add(onScreenLvl);
        showImage(onScreenLvl);
        Integer lvl = onScreenLvl + 1;
        textlvlNumber.setText((lvl).toString());
        saveData();


    }


    public void showImage(final int i) {
        final Namedatabase answers = new Namedatabase();



        image = findViewById(R.id.imageView);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();

        storageRef.child("Animals/" + answers.getfilenames(difficulty)[i]).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.get().load(uri).into(image);}







    });


    }
    private void goToStartActiviy(){
        Intent StartingActivityIntent = new Intent(MainActivity.this,StartingActivity.class);
        startActivity(StartingActivityIntent);
        finish();
    }
}
