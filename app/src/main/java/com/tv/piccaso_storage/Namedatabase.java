package com.tv.piccaso_storage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Namedatabase {


    public String[][] getAnswers(String difficulty) {


        switch (difficulty) {

            case "Easy":
                String[][] beginner = {{"shark"}, {"elephant","Elephant"}, {"penguin","penguins"}, {"lion"}, {"fox"}, {"dolphin"}, {"deer"}, {"crocodile"}, {"panda","giant panda"},{"tiger"}};//in future add name with capitals
                return beginner;

            case "Intermediate":
                String[][] intermediate = {{"beluga"}, {"alpaca"}, {"ara","macaw"}, {"antelope"}, {"sloth"}, {"hawk"}, {"kangaroo"}, {"octopus"}, {"orangutan"},{"beaver"}};
                return intermediate;


            default:
                return null;


        }
    }

    public String[][] getDescriptions(String difficulty) {

        switch (difficulty) {
            case "Easy":
                String[][] beginner = {{""}, {""}, {""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""}};
                return beginner;

            case "Intermediate":
                String[][] intermediate = {{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},{""},};
                return intermediate;


            default:
                return null;
        }
    }
    public String[] getfilenames(String difficulty) {

        switch (difficulty) {
            case "Easy":
                String[] beginner = {"shark.jpg", "elephant.jpg", "penguin.jpg", "lion.jpg", "fox.jpg", "dolphin.jpg", "deer.jpg", "crocodile.jpg", "panda.jpg","tiger.jpg"};
                return beginner;

            case "Intermediate":
                String[] intermiediate = {"beluga.png", "alpaca.jpg", "ara.png", "antelope.jpg", "sloth.jpeg", "hawk.jpg", "cangaroo.jpg", "octopus.jpg", "orangutan.jpg","beaver.jpg"};
                return intermiediate;


            default:
                return null;
        }
    }
}

